require 'rails_helper'

RSpec.describe ImportService do
  describe 'importing terms' do
    describe '#call' do
      let(:body) do
        JSON.parse(File.read('spec/requests/terms.json'))['academicTerm']
      end

      before do
        stub_request(:get, 'https://ws.muohio.edu/academicTerms').with(
          headers: {
            'Accept' => 'application/json',
            'Content-Type' => 'application/json'
          }
        ).to_return body: body
      end

      it 'creates all the terms from the json response' do
        expect { ImportService.call }.to change { Term.count }.by body.count
      end

      it 'populates the fields correctly' do
        ImportService.call
        term = Term.first
        expect(term.name).to eq body[0]['name']
        expect(term.original_id).to eq body[0]['termId'].to_i
        expect(term.end_date).to eq Date.parse(body[0]['endDate'])
        expect(term.start_date).to eq Date.parse(body[0]['startDate'])
      end
    end

    #describe 'importing courses' do
    #  let(:subject) { 'CSE' }
    #  let(:term) { { "termId" => '201820'} }
    #  let(:body) do
    #    JSON.parse(File.read('spec/requests/sections.json'))['courseSections']
    #  end
    #
    #  before do
    #    stub_request(:get, "http://ws.miamioh.edu/courseSectionV2/#{term["termId"]}.json?campusCode=O&courseSubjectCode=#{subject}").with(
    #      headers: {
    #        'Accept' => 'application/json',
    #        'Content-Type' => 'application/json'
    #      }
    #    ).to_return body: body
    #  end
    #
    #  it { expect { ImportService.import_sections_for_subject(term, subject) }.to change { Term.count } }
    #end
  end
end
