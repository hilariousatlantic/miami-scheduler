# Miami Scheduler

[![Travis Build Status](https://travis-ci.org/HilariousAtlantic/miami-scheduler-web.svg?branch=master)](https://travis-ci.org/HilariousAtlantic/miami-scheduler-api)

[![Pipelines Build Status](https://pipelines-badges-service.useast.staging.atlassian.io/badge/hilariousatlantic/miami-scheduler-api.svg)](https://bitbucket.org/hilariousatlantic/miami-scheduler-api/addon/pipelines/home)



## Setup locally

### Postgres
```
brew install postgres
```

### Ruby via [rbenv](https://github.com/rbenv/rbenv)
```
brew install rbenv
rbenv init
source ~/.bashrc
rbenv install 2.4.2
gem install bundler
```

### Setup the Rails project
```
bundle install
bundle exec rails db:setup
```

### Run tests
```
bundle exec rspec
```

### Run the server
```
rails s
```

## Continuous Integration
Push to master to run the tests on travis.  To test on bitbucket piplines:
```
git remote add bitbucket git@bitbucket.org:hilariousatlantic/miami-scheduler.git
git push bitbucket master
```
