class ImportService
  @@subjects = JSON.parse(File.read(File.join(File.dirname(__FILE__), 'subjects.json')))

  def self.call
    terms = fetch_terms
    import_terms(terms)

    #sections = []
    #terms.each { |term| sections.concat fetch_sections(term) }

    #import_sections(sections)
  end

  def self.fetch_sections(term)
    sections = []
    @@subjects.each do |subject|
      sections.concat import_sections_for_subject(term, subject)
    end
    sections
  end

  def self.import_sections_for_subject(term, subject)
    headers = {
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    }
    response = HTTParty.get(
      "http://ws.miamioh.edu/courseSectionV2/#{term["termId"]}.json?campusCode=O&courseSubjectCode=#{subject}",
      headers: headers)
    response.body
  end

  def self.fetch_courses(terms)
    headers = {
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    }
    response = HTTParty.get(
      'https://ws.muohio.edu/academicTerms',
      headers: headers)
    response.body
  end

  def self.import_terms(terms)
    terms.each do |term|
      Term.find_or_create_by(
        name: term['name'],
        end_date: term['endDate'],
        original_id: term['termId'],
        start_date: term['startDate']
      )
    end
  end

  def self.fetch_terms
    headers = {
      'Accept' => 'application/json',
      'Content-Type' => 'application/json'
    }
    response = HTTParty.get(
      'https://ws.muohio.edu/academicTerms',
      headers: headers)
    response.body
  end
end
