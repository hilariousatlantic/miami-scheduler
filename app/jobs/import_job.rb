require 'httparty'

class ImportJob < ApplicationJob
  queue_as :default

  def perform(*args)
    import_terms(fetch_terms)
  end

  private

    def import_terms(data)
    end

    def fetch_terms
      headers = { "Accept"  => "application/json", "Content-Type"  => "application/json" }
      response = HTTParty.get("https://ws.muohio.edu/academicTerms", :headers => headers)
      JSON.parse(response.body, :symbolize_names => true)
    end
end
