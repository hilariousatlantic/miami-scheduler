class AddFieldsToTerm < ActiveRecord::Migration[5.1]
  def change
    add_column :terms, :start_date, :date
    add_column :terms, :end_date, :date
    add_column :terms, :original_id, :integer
  end
end
